## Introduction

Reads mesh information and calculates topological information about that mesh along
with geometric information.

## Building 

CMake is used to build this project. To build:

    $> cmake <path to mesh>
    $> make prereq
    $> make

## Some useful options

Other useful options for cmake include

    -DCMAKE_BUILD_TYPE=<Debug|Release>

Enable 'Debug' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=1"

Enable 'Info' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=2"

Enable 'Warn' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=3"

Enable 'Error' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=4"

Enable 'Severe error' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=5"

By default 'Warn' level messages are used and so if just `-DDEBUG_MESSAGES` is
used without `-DDEBUG_MESSAGES_LEVEL` it will revert to level 3 (warnings). 

BY default tests are built. You can disable building of tests using

    -DBUILD_TESTING=OFF



