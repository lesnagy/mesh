#
# Library configuration file used by dependent projects.
# @file:   mesh-config.cmake
# @author: L. Nagy
#

if (NOT DEFINED mesh_FOUND)

  # Locate the library headers.
  find_path(mesh_include_dir
    NAMES PatranLoader.h
          TecplotLoader.h
          global_constants.h
          patran.h
          tecplot.h
    PATHS ${mesh_DIR}/include
  )

  # Locate libraries.
  find_library(mesh_libraries
    mesh
    PATHS ${mesh_DIR}/lib
  )

#  # Export libary targets.
#  set(mesh_libraries
#    mesh
#    CACHE INTERNAL "mesh library" FORCE
#  )

  # Handle REQUIRED, QUIED and version related arguments to find_package(...)
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(
    mesh DEFAULT_MSG
    mesh_include_dir
    mesh_libraries
  )

#  # If the project is not `mesh` then make directories. 
#  if (NOT ${PROJECT_NAME} STREQUAL mesh)
#    add_subdirectory(
#      ${mesh_DIR}
#      ${CMAKE_CURRENT_BINARY_DIR}/mesh
#    )
#  endif()
endif()
