#ifndef MESHLIB_ELEMENT_HPP_
#define MESHLIB_ELEMENT_HPP_

#include <array>

#include "mesh/Vertex.hpp"

namespace meshlib {

namespace mesh {

template <unsigned int N> class Element
{
public:

  Element() {
    for (size_t i = 0; i < N; ++i) {
      mVertIdxs[i] = 0;
      mVerts[i]    = 0.0;
    }
  }

  Element(const unsigned int (&idxs) [N],
          const Vertex<N-1>  (&verts) [N])
  {
    for (size_t i = 0; i < N; ++i) {
      mVertIdxs[i] = idxs[i];
      mVerts[i] = verts[i];
    }
  }

  const Vertex<N-1> & vertex(size_t idx) 
  {
    return mVerts[idx];
  }
  
  unsigned int gindex(size_t idx) 
  {
    return mVertIdxs[idx];
  }

private:
  std::array< unsigned int, N > mVertIdxs;
  std::array< Vertex<N-1>, N > mVerts;
};

} // namespace mesh

} // namespace meshlib
 

#endif // MESHLIB_ELEMENT_HPP_
