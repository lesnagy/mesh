#ifndef MESHLIB_MESH_HPP_
#define MESHLIB_MESH_HPP_

#include <tuple>
#include <unordered_set>
#include <iostream>

#include "topology/Types.hpp"
#include "topology/SConn.hpp"
#include "topology/SConnNm1_To_SConnN_Index_Set.hpp"
#include "topology/SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity.hpp"
#include "topology/SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity.hpp"
#include "topology/SConnN_Boundary.hpp"

#include "mesh/Vertex.hpp"

namespace meshlib {

namespace mesh {

template <unsigned int N> class Mesh
{
public:
  Mesh()
  {}

  void reset() 
  {
    mVert.clear();
    mFaceToElementIndexSet.clear();
    mVertexToVertexMap.clear();
    mElementToElementMap.clear();
    mBoundaryFaces.clear();
    mBFaceToBElementIndexSet.clear();
    mBVertexToBVertexMap.clear();
    mBElementToBElementMap.clear();
  }

  void begin() 
  {
    reset();
  }

  void vertex(Vertex<N-1> v)
  {
    mVert.push_back(v);
  }

  void vertex(const double (&coord) [N-1])
  {
    mVert.push_back(Vertex<N-1>(coord));
  }

  void sconn(topology::SConn<N> e) 
  {
    mFaceToElementIndexSet.insert(e);
  }

  void end() 
  {
    mVertexToVertexMap = mVertexToVertexFun(mFaceToElementIndexSet);

    mElementToElementMap = mElementToElementFun(mFaceToElementIndexSet);

    mBoundaryFaces = mBoundaryFun(mFaceToElementIndexSet);

    for (auto p : mBoundaryFaces) {
      mBFaceToBElementIndexSet.insert(p);
    }

    mBVertexToBVertexMap = mVertexToVertexFun(mBFaceToBElementIndexSet);

    mBElementToBElementMap = mElementToElementFun(mBFaceToBElementIndexSet);
  }

  const topology::SConn<N> & element(size_t idx)
  {
    return mFaceToElementIndexSet.sconns()[idx];
  }

  const topology::SConn<N-1> & 
    boundaryElement(size_t idx) 
  {
    return mBoundaryFaces[idx];
  }

  const Vertex<N-1> & vertex(size_t idx) const
  {
    mVert[idx];
  }

  bool isBoundaryVertexIndex(size_t idx) const
  {
    if (mBVertexToBVertexMap.find(idx) != mBVertexToBVertexMap.end()) {
      return true;
    }
    return false;
  }

  /**
   * Retrieve the neighbours of the element with index 'idx'
   */
  const std::set< unsigned int> &
    neighbourElementIndices(unsigned int idx) const
  {
    return mElementToElementMap.at(idx);
  }

  /**
   * Retrieve the neighbours of the boundary element with index 'idx'
   */
  const std::set< unsigned int > &
    boundaryNeighbourElementIndices(unsigned int idx) const
  {
    return mBElementToBElementMap.at(idx);
  }

  const std::set< unsigned int > &
    neighbourVertexIndices(size_t idx) const
  {
    return mVertexToVertexMap.at(idx);
  }

  const std::set< unsigned int > & 
    boundaryNeighbourVertexIndices(size_t idx) const
  {
    if (!isBoundaryVertexIndex(idx)) {
      return mEmptySet;
    }
    return mBVertexToBVertexMap.at(idx);
  }

  const std::vector< Vertex<N-1> > & vertices() const
  {
    return mVert;
  }

  const topology::int_to_intset & vertexNeighbours() const
  {
    return mVertexToVertexMap;
  }

  const std::vector< topology::SConn<N> > & elements() const
  {
    return mFaceToElementIndexSet.sconns();
  }

  const topology::int_to_intset & elementNeighbours() const
  {
    return mElementToElementMap;
  }

  const std::vector< topology::SConn<N-1> > & boundaryElements() const
  {
    return mBoundaryFaces;
  }

  const topology::int_to_intset & boundaryElementNeighbours() const
  {
    return mBElementToBElementMap;
  }

  const topology::int_to_intset & boundaryVertexNeighbours() const
  {
    return mBVertexToBVertexMap;
  }

private:
  std::vector< Vertex<N-1> > mVert;

  topology::SConnNm1_To_SConnN_Index_Set<N> mFaceToElementIndexSet;
  
  topology::SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity mVertexToVertexFun;

  topology::int_to_intset mVertexToVertexMap;

  topology::SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity mElementToElementFun;

  topology::int_to_intset mElementToElementMap;
  
  topology::SConnN_Boundary mBoundaryFun;

  std::vector< topology::SConn<N-1> > mBoundaryFaces;

  topology::SConnNm1_To_SConnN_Index_Set<N-1> mBFaceToBElementIndexSet;

  topology::int_to_intset mBVertexToBVertexMap;

  topology::int_to_intset mBElementToBElementMap;

  std::set<unsigned int> mEmptySet;
};

} // namespace mesh

} // namespace meshlib

#endif // MESHLIB_MESH_HPP_
