#ifndef MESHLIB_VERTEX_HPP_
#define MESHLIB_VERTEX_HPP_

#include <array>

namespace meshlib {

namespace mesh {

template <unsigned int N> class Vertex
{
public:

  Vertex() {
    for (size_t i = 0; i < N; ++i) {
      mComps[i] = 0.0;
    }
  }

  Vertex(const double (&comps) [N])
  {
    for (size_t i = 0; i < N; ++i) {
      mComps[i] = comps[i];
    }
  }

  Vertex(const Vertex &rhs)
  {
    mComps = rhs.mComps;
  }

  ~Vertex() {}

  Vertex & operator = (const Vertex & rhs)
  {
    Vertex tmp(rhs);
    tmp.swap(*this);
    return *this;
  }

  double operator[] (size_t i) const
  {
    return mComps[i];
  }

  double get(size_t i)  const
  {
    return mComps[i];
  }

  void set(size_t i, double v) 
  {
    mComps[i] = v;
  }
  
private:
  std::array<double, N> mComps;
};

} // namespace meshlib 

} // namespace mesh
  


#endif // MESHLIB_VERTEX_HPP_
