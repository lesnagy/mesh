#ifndef MESHLIB_TOPOLOGY_CHOOSE_HPP_
#define MESHLIB_TOPOLOGY_CHOOSE_HPP_

#include "DebugMacros.h"

namespace meshlib {

namespace topology {

template < int N, int K >
struct Choose {
	enum { nCk = Choose<N-1, K-1>::nCk + Choose<N-1, K>::nCk };
};
template < int N > struct Choose<N, 0> { enum { nCk = 1 }; };
template < int N > struct Choose<N, N> { enum { nCk = 1 }; };

} // namespace topology

} // namespace meshlib

#endif // MESHLIB_TOPOLOGY_CHOOSE_HPP_
