#ifndef MESHLIB_TOPOLOGY_COMBINATORICS_HPP_
#define MESHLIB_TOPOLOGY_COMBINATORICS_HPP_

#include <cstddef>

#include "DebugMacros.h"

namespace meshlib {

namespace topology {

unsigned int next_combination(unsigned int *ar, size_t n, unsigned int k);

} // namespace topology

} // namespace meshlib

#endif  // MESHLIB_TOPOLOGY_COMBINATORICS_HPP_
