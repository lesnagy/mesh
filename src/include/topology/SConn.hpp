#ifndef MESHLIB_TOPOLOGY_SCONN_HPP_
#define MESHLIB_TOPOLOGY_SCONN_HPP_

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "DebugMacros.h"

#include "Hashing.h"

#include "topology/Choose.hpp"
#include "topology/Combinatorics.hpp"
#include "topology/StaticSort.hpp"

namespace meshlib {

namespace topology {

/**
 * A simplex connectivity object of degree \f$N\f$ is denoted \f$SConn_N\f$. It 
 * represents \f$N\f$ indices that refer to the \f$N\f$ vertices that comprise
 * the simplex. The indices of an \f$SConn_N\f$ object are always sorted (unlike the 
 * Tuple), this gives each \f$SConn_N\f$ a unique 'name'.
 */
template <unsigned int N> class SConn
{
public:
  friend class SConn<N+1>;

  /**
   * Default constructor creates an \f$SConn_N\f$ object with \f$N\f$ zeros 
   * for indices (including all sub \f$SConn_{N-1}\f$ objects).
   */
  SConn() {
    mIdxs.fill(0);
    mParent = NULL;
    mInternalIdx = -1;
    mMaterialIndex = 0;
  }

  /**
   * Overloaded constructor creates an \f$SConn_N\f$ object with \f$N\f$ zeros 
   * for indices (including all sub \f$SConn_{N-1}\f$ objects) with given 
   * material index.
   */
  SConn(unsigned int materialIndex) {
    mIdxs.fill(0);
    mParent = NULL;
    mInternalIdx = -1;
    mMaterialIndex = materialIndex;
  }

  /**
   * Overloaded constructor creates an \f$SConn_N\f$ object from a list of 
   * \f$N\f$ integers.
   */
  SConn(const unsigned int (& idxs) [N])
  {
    for (size_t i = 0; i < N; ++i) {
      mIdxs[i] = idxs[i];
    }
    sort();
    mParent = NULL;

    mInternalIdx = -1;

    mMaterialIndex = 0;
  }

  /**
   * Overloaded constructor creates an \f$SConn_N\f$ object from a list of 
   * \f$N\f$ integers with given material index.
   */
  SConn(const unsigned int (& idxs) [N], unsigned int materialIndex)
  {
    for (size_t i = 0; i < N; ++i) {
      mIdxs[i] = idxs[i];
    }
    sort();
    mParent = NULL;

    mInternalIdx = -1;

    mMaterialIndex = materialIndex;
  }

  /**
   * Copy constructor.
   */
  SConn(const SConn &rhs)
  {
    mIdxs = rhs.mIdxs;
    mParent = rhs.mParent;
    mInternalIdx = rhs.mInternalIdx;
    mMaterialIndex = rhs.mMaterialIndex;
  }

  /**
   * Destructor.
   */
  ~SConn() {}

  /////////////////////////////////////////////////////////////////////////////
  // Operators                                                               //
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Assignemnt operator, assigns the value of an \f$SConn_N\f$ object to this
   * one.
   */
  SConn & operator = (const SConn & rhs) 
  {
    SConn tmp(rhs);
    tmp.swap(*this);
    //mParent = rhs.mParent;
    //mInternalIdx = rhs.mInternalIdx;
    //mMaterialIndex = rhs.mMaterialIndex;
    return *this;
  }

  /**
   * Equality operator, checks if two \f$SConn_N\f$ objects are the same.
   */
  bool operator == (const SConn & rhs) const
  {
    return std::equal(mIdxs.begin(), mIdxs.end(), rhs.mIdxs.begin());
  }

  /**
   * Equality operator, checks if this \f$SConn_N\f$ object is the same as a
   * list of \f$N\f$ indices.
   */
  bool operator == (const std::vector<unsigned int> & rhs) const
  {
    return std::equal(mIdxs.begin(), mIdxs.end(), rhs.begin());
  }

  /**
   * Inequality operator, checks if two \f$SConn_N\f$ objects are unequal.
   */
  bool operator != (const SConn & rhs) const  
  {
    return !operator==(rhs);
  }

  /**
   * Inequality operator, checks if this \f$SConn_N\f$ object is unequal to a 
   * list of \f$N\f$ indices.
   */
  bool operator != (const std::vector<unsigned int> & rhs) const
  {
    return !operator==(rhs);
  }

  /**
   * Index operator, returns the reference to the value at the given input 
   * index.
   */
  inline unsigned int & operator[] (size_t i) 
  { 
    return  mIdxs[i]; 
  }

  /**
   * Function to retrieve a reference to the value at the given input index.
   */
  inline unsigned int & get (size_t i) 
  { 
    return  mIdxs[i];
  }

  /**
   * Function to retrieve a the value at the given input index (not a
   * reference).
   */
  inline unsigned int getc (size_t i) const 
  { 
    return  mIdxs[i];
  }

  /**
   * For a given \f$SConn_N\f$ object, return all the \f$SConn_{N-1}\f$ objects
   * belonging to this obect, that is all the simplex connectivity objects with
   * \f$N-1\f$ indices.
   */
  std::vector< SConn<N-1> > subConns() const
  {
    static_assert(N-1 != 0, 
        "Cannot instantiate SConn<0> object (corresponding to [N-1]-simplex)");

    // There will be M SConn<N-1> objects produced.
    std::vector< SConn<N-1> > output(M);

    // Each SConn<N-1> object has 'this' object as its parent.
    for (size_t i = 0; i < output.size(); ++i) {
      output[i].mParent = this;
    }

    // Calculate the permutations of 'this' parent's mIdxs that will go in to
    // the children.
    std::vector<unsigned int> indices(N-1);
    for (size_t i = 0; i < N-1; ++i) {
      indices[i] = i;
    }

    size_t j = 0; 
    do {
      for (size_t i = 0; i < N-1; ++i) {
        output[j][i] = mIdxs[indices[i]];
      }
      j = j + 1;
    } while(next_combination(&indices[0], N, N-1));

    // Each of the children will get this parent's materialId, and will be told
    // to compute the appropriate internal id.
    for (SConn<N-1> & e : output) {
      e.computeInternalIndex();
      e.mMaterialIndex = mMaterialIndex;
    }

    return output;
  }

  typename std::array<unsigned int, N>::iterator 
  begin()
  {
    return mIdxs.begin();
  }

  typename std::array<unsigned int, N>::const_iterator 
  begin() const
  {
    return mIdxs.begin();
  }

  typename std::array<unsigned int, N>::const_iterator 
  cbegin() const
  {
    return mIdxs.cbegin();
  }

  typename std::array<unsigned int, N>::iterator 
  end()
  {
    return mIdxs.end();
  }

  typename std::array<unsigned int, N>::const_iterator 
  end() const
  {
    return mIdxs.end();
  }

  typename std::array<unsigned int, N>::const_iterator 
  cend() const
  {
    return mIdxs.cend();
  }

  /////////////////////////////////////////////////////////////////////////////
  // Other functions.                                                        //
  /////////////////////////////////////////////////////////////////////////////

  void swap(SConn & rhs) 
  {
    mIdxs.swap(rhs.mIdxs);
  }

  /**
   * Return the number of indices being held by this \f$SConn_N\f$ object, 
   * i.e. the degree (which is just equal to \f$N\f$).
   */
  size_t size()
  {
    return N;
  }
  
  /**
   * Return a string representation of this \f$SConn_N\f$ object.
   */
  std::string str() const
  {
    std::stringstream ss;

    ss << "(";
    for (size_t i = 0; i < N; ++i) {
      if (i != N-1) {
        ss << mIdxs[i] << ", ";
      } else {
        ss << mIdxs[i];
      }
    }
    ss << ")";

    ss << " mat:" << mMaterialIndex;

    if (hasParent()) {
      ss << " iid:" << mInternalIdx;
    } else {
      ss << " iid: NO PARENT";
    }

    return ss.str();
  }

  const SConn<N+1>* parent() { return mParent; }

  std::set<unsigned int> indicesAsSet() const { 
    std::set<unsigned int> output;
    for (auto v : mIdxs) {
      output.insert(v);
    }
    
    return output;
  }

  bool hasParent() const {
    if (mParent == NULL) {
      return false;
    }
    return true;
  }

  unsigned int internalIndex()
  {
    return mInternalIdx;
  }

private:

  /////////////////////////////////////////////////////////////////////////////
  // Private member variables                                                //
  /////////////////////////////////////////////////////////////////////////////

  static const unsigned int M = Choose<N,N-1>::nCk;
 
  std::array<unsigned int, N>  mIdxs;

  StaticSort<N> staticSort;

  const SConn<N+1> *mParent;

  unsigned int mInternalIdx;

  int mMaterialIndex;

  /////////////////////////////////////////////////////////////////////////////
  // Private functions                                                       //
  /////////////////////////////////////////////////////////////////////////////
 
  void sort()
  {
    staticSort(mIdxs);
  }

  void computeInternalIndex() 
  {
    if (hasParent()) {
      // Get the vertex index that is part of the element, but not the face.
      auto parentIdxsSet = parent()->indicesAsSet();
      auto childIdxsSet  = indicesAsSet();

      std::set<unsigned int> diffSet;

      std::set_difference(parentIdxsSet.begin(), parentIdxsSet.end(),
                          childIdxsSet.begin(),  childIdxsSet.end(),
                          std::inserter(diffSet, diffSet.begin()));

      assert( diffSet.size() == 1 );

      mInternalIdx = *diffSet.begin();
    }
  }

};

} // namespace topology

} // namespace meshlib

// Inject hash for SConn<N> in to std namespace.
namespace std
{

template <unsigned int N> struct hash< meshlib::topology::SConn<N> >
{
  size_t operator() (const meshlib::topology::SConn<N> & conns) const
  {
    size_t seed = 0;
    for (size_t i = 0; i < N; ++i) {
      hashing::hash_combine(seed, conns.getc(i));
    }

    return seed;
  }
};

// Overload the redirect operator to output an SConn<N> object
template <unsigned int N>
std::ostream & operator<< (std::ostream & o, 
                           const meshlib::topology::SConn<N> sconn)
{
  return o << sconn.str();
}

} // namespace std

#endif // MESHLIB_TOPOLOGY_SCONN_HPP_
