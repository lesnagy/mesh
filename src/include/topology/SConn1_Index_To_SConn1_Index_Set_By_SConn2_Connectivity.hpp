#ifndef MESHLIB_TOPOLOGY_SCONN1_INDEX_TO_SCONN1_INDEX_SET_BY_SCONN2_CONNECTIVITY_HPP_
#define MESHLIB_TOPOLOGY_SCONN1_INDEX_TO_SCONN1_INDEX_SET_BY_SCONN2_CONNECTIVITY_HPP_

#include <cstddef>
#include <unordered_map>

#include "DebugMacros.h"

#include "topology/Types.hpp"
#include "topology/SConnNm1_To_SConnN_Index_Set.hpp"

namespace meshlib {
  
namespace topology {

class SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity 
{
public:
  template <unsigned int N>
  int_to_intset operator() (const SConnNm1_To_SConnN_Index_Set<N> & imap)
  {
    int_to_intset omap;

    for (auto fe : imap) {
      auto fconn = fe.first;
      auto eidxs = fe.second;

      for (auto sconnId : eidxs) {
        const SConn<N> & sconn = imap.sconn(sconnId);
        for (auto vii : sconn) {
          for (auto vij : sconn) {
            if (vii != vij) {
              omap[vii].insert(vij);
              omap[vij].insert(vii);
            }
          }
        }
      }
    }

    return omap;
  }

};

} // namespace topology

} // namespace meshlib

#endif // MESHLIB_TOPOLOGY_SCONNN_INDEX_TO_SCONNN_INDEX_SET_BY_SCONNNM1_CONNECTIVITY_HPP_
