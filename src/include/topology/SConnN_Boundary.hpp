#ifndef MESHLIB_TOPOLOGY_SCONNN_BOUNDARY_HPP_
#define MESHLIB_TOPOLOGY_SCONNN_BOUNDARY_HPP_

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iterator>
#include <utility>

#include "DebugMacros.h"

#include "topology/Types.hpp"
#include "topology/SConn.hpp"
#include "topology/SConnNm1_To_SConnN_Index_Set.hpp"

namespace meshlib {

namespace topology {

class SConnN_Boundary
{
public:
  /**
   * Function to extract face/element info on the boundary of a mesh.
   * \param imap a list of mappings from \f$SConn_{N-1}\f$ objects to 
   *             \f$SConn_N\f$ index sets, i.e. 
   *             \f$SConn_{N-1} \rightarrow \{\mathcal{I}(SConn_{N})\}\f$. 
   */
  template <unsigned int N>
  std::vector< SConn<N-1> >
  operator() (const SConnNm1_To_SConnN_Index_Set<N> & imap)
  {
    std::vector< SConn<N-1> > output;

    for (auto fe : imap) {
      auto fconn = fe.first;
      auto eidxs = fe.second;

      assert( (1 <= eidxs.size()) and (eidxs.size() <= 2) );

      if (eidxs.size() == 1) {
        output.push_back(fconn);
      }
    }

    return output;
  }
};

} // namespace topology

} // namespace meshlib

#endif  // MESHLIB_TOPOLOGY_SCONNN_BOUNDARY_HPP_
