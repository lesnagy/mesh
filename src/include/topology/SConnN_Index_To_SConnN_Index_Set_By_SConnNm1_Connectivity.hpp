#ifndef MESHLIB_TOPOLOGY_SCONNN_INDEX_TO_SCONNN_INDEX_SET_BY_SCONNNM1_CONNECTIVITY_HPP_
#define MESHLIB_TOPOLOGY_SCONNN_INDEX_TO_SCONNN_INDEX_SET_BY_SCONNNM1_CONNECTIVITY_HPP_

#include <cassert>
#include <iostream>
#include <iomanip>

#include "DebugMacros.h"

#include "topology/Types.hpp"
#include "topology/SConn.hpp"
#include "topology/SConnNm1_To_SConnN_Index_Set.hpp"

namespace meshlib {

namespace topology {

class SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity 
{
public:
  template <unsigned int N>
  int_to_intset operator() (const SConnNm1_To_SConnN_Index_Set<N> & imap) 
  {
    int_to_intset omap;

    for (auto fe : imap) {
      auto fconn = fe.first;
      auto eidxs = fe.second;

      assert( (1 <= eidxs.size()) and (eidxs.size() <= 2) );

      if (eidxs.size() == 2) {
        // No. of elements incident to face 'fconn' is 2. This is an internal
        // face/element.
        unsigned int eidx0 = *(eidxs.begin());
        unsigned int eidx1 = *(eidxs.rbegin());
        
        omap[eidx0].insert(eidx1);
        omap[eidx1].insert(eidx0);
      }
    }

    return omap;
  }
};

} // namespace topology

} // namespace meshlib

#endif // MESHLIB_TOPOLOGY_SCONNN_INDEX_TO_SCONNN_INDEX_SET_BY_SCONNNM1_CONNECTIVITY_HPP_
