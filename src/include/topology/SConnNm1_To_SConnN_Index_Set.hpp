#ifndef MESHLIB_TOPOLOGY_SCONNNM1_TO_SCONNN_INDEX_SET_HPP_
#define MESHLIB_TOPOLOGY_SCONNNM1_TO_SCONNN_INDEX_SET_HPP_

#include <cstddef>
#include <set>
#include <unordered_map>

#include "DebugMacros.h"

#include "topology/SConn.hpp"
#include "topology/Tuple.hpp"

namespace meshlib {

namespace topology {

template <unsigned int N>
class SConnNm1_To_SConnN_Index_Set
{
public:

  void insert(const SConn<N> & elementConn) 
  {
    DEBUG("Adding element: " << elementConn);
    std::vector< SConn<N-1> > faceConns = elementConn.subConns();

    // Get the new elementId
    unsigned int elementId = mElementMap.size();

    for (auto faceConn : faceConns) {
      DEBUG("Adding face: " << faceConn);
      // For each of the face conns in the element conn.
      if (mFEMap.find(faceConn) != mFEMap.end()) {
        DEBUG("FOUND: " << faceConn << ", in lookup");
        // If 'faceConn' is already present, add elementId to its set.
        mFEMap[faceConn].insert(elementId);
        DEBUG("ADDED!");
      } else {
        // Else if 'faceConn' is not present, add it and create a new set.
        DEBUG("COULD NOT FIND: " << faceConn << ", in lookup");
        mFEMap.insert({faceConn, {elementId}});
        DEBUG("CREATED!");
      }
    }
    mElementMap.push_back(elementConn);
  }

  typename std::unordered_map< SConn<N-1>, std::set<unsigned int> >::iterator 
  begin() 
  {
    return mFEMap.begin();
  }

  typename std::unordered_map< SConn<N-1>, std::set<unsigned int> >::const_iterator 
  begin() const
  {
    return mFEMap.begin();
  }

  typename std::unordered_map< SConn<N-1>, std::set<unsigned int> >::const_iterator 
  cbegin() const
  {
    return mFEMap.cbegin();
  }

  typename std::unordered_map< SConn<N-1>, std::set<unsigned int> >::iterator 
  end() 
  {
    return mFEMap.end();
  }

  typename std::unordered_map< SConn<N-1>, std::set<unsigned int> >::const_iterator 
  end() const
  {
    return mFEMap.end();
  }

  typename std::unordered_map< SConn<N-1>, std::set<unsigned int> >::const_iterator 
  cend() const
  {
    return mFEMap.cend();
  }

  void clear()
  {
    mFEMap.clear();
    mElementMap.clear();
  }

  size_t size()
  {
    return mFEMap.size();
  }

  const SConn<N> & sconn(size_t i) const
  {
    return mElementMap[i];
  }

  const std::vector< SConn<N> > & sconns() const
  {
    return mElementMap;
  }

private:
  std::unordered_map< SConn<N-1>, std::set<unsigned int> > mFEMap;
  std::vector< SConn<N> > mElementMap;
};

} // namespace topology

} // namespace meshlib

#endif // MESHLIB_TOPOLOGY_SCONNNM1_TO_SCONNN_INDEX_SET_HPP_
