#ifndef MESHLIB_TOPOLOGY_TUPLE_HPP_
#define MESHLIB_TOPOLOGY_TUPLE_HPP_

#include <algorithm>
#include <cstddef>
#include <sstream>
#include <string>
#include <vector>

#include "DebugMacros.h"

#include "Hashing.h"

namespace meshlib {

namespace topology {

/**
 * A tuple of \f$N\f$ indices is denoted \f$Tuple_N\f$. It is similar to the 
 * SConn class except that the indices are not sorted (and so they retain 
 * their original ordering). There is also no concept of sub-tuple objects as
 * is the case with SConn objects.
 */
template <unsigned int N>
class Tuple
{
public:

  /**
   * Default constructor creates a \f$Tuple_N\f$ object with zeros for indices
   * (including all sub \f$Tuple_N\f$ objects).
   */
  Tuple() : mIdxs(N, 0u) {}

  /**
   * Overloaded constructor creates a \f$Tuple_N\f$ object from a list.
   */
  Tuple(const unsigned int (& idxs) [N]) : mIdxs(&idxs[0], &idxs[N])
  {}

  /**
   * Copy constructor.
   */
  Tuple(const Tuple &rhs)
  {
    mIdxs = rhs.mIdxs;
  }

  /**
   * Destructor.
   */
  ~Tuple() {}

  /////////////////////////////////////////////////////////////////////////////
  // Operators                                                               //
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Assignment operator, assigns the value of rhs to this object.
   */
  Tuple & operator = (const Tuple & rhs) 
  {
    Tuple tmp(rhs);
    tmp.swap(*this);
    return *this;
  }

  /**
   * Equality operator, checks if two \f$Tuple_N\f$ objects are the same.
   */
  bool operator == (const Tuple & rhs) const
  {
    return std::equal(mIdxs.begin(), mIdxs.end(), rhs.mIdxs.begin());
  }

  /**
   * Equality operator, checks if this \f$Tuple_N\f$ is the same as a list of indices.
   */
  bool operator == (const std::vector<unsigned int> & rhs) const
  {
    return std::equal(mIdxs.begin(), mIdxs.end(), rhs.begin());
  }

  /**
   * Inequality operator, checks if two \f$Tuple_N\f$ objects are unequal.
   */
  bool operator != (const Tuple & rhs) const  
  {
    return !operator==(rhs);
  }

  /**
   * Inquality operator, checks if this \f$Tuple_N\f$ is unequal to a list of indices.
   */
  bool operator != (const std::vector<unsigned int> & rhs) const
  {
    return !operator==(rhs);
  }

  /**
   * Index operator, returns the reference to the value at the given input 
   * index.
   */
  inline unsigned int & operator[] (size_t i) 
  { 
    return  mIdxs[i]; 
  }

  /**
   * Function to retrieve a reference to the value at the given input index.
   */  
  inline unsigned int & get (size_t i) 
  { 
    return  mIdxs[i];
  }

  /**
   * Function to retrieve a the value at the given input index (not a
   * reference).
   */
  inline unsigned int getc (size_t i) const 
  { 
    return  mIdxs[i];
  }

  std::vector<unsigned int>::iterator begin()
  {
    return mIdxs.begin();
  }

  std::vector<unsigned int>::const_iterator begin() const
  {
    return mIdxs.begin();
  }

  std::vector<unsigned int>::const_iterator cbegin() const
  {
    return mIdxs.cbegin();
  }

  std::vector<unsigned int>::iterator end()
  {
    return mIdxs.end();
  }

  std::vector<unsigned int>::const_iterator end() const
  {
    return mIdxs.end();
  }

  std::vector<unsigned int>::const_iterator cend() const
  {
    return mIdxs.cend();
  }

  /////////////////////////////////////////////////////////////////////////////
  // Other functions.                                                        //
  /////////////////////////////////////////////////////////////////////////////

  void swap(Tuple & rhs) 
  {
    mIdxs.swap(rhs.mIdxs);
  }

  /**
   * Return the number of indices being held by this \f$Tuple_N\f$ object, 
   * i.e. the degree (which is just equal to \f$N\f$).
   */
  size_t size()
  {
    return N;
  }
  
  /**
   * Return a string representation of this \f$Tuple_N\f$ object.
   */
  std::string str() const
  {
    std::stringstream ss;

    ss << "{";
    for (size_t i = 0; i < N; ++i) {
      if (i != N-1) {
        ss << mIdxs[i] << ", ";
      } else {
        ss << mIdxs[i];
      }
    }
    ss << "}";

    return ss.str();
  }

private:

  ///////////////////////////////////////////////////////////////////////////
  // Private member variables
  ///////////////////////////////////////////////////////////////////////////

  std::vector<unsigned int>  mIdxs;
};

} // namespace topology

} // namespace meshlib

// Inject hash for Tuple<N> in to std namespace.
namespace std
{

template <unsigned int N> struct hash< meshlib::topology::Tuple<N> >
{
  size_t operator() (const meshlib::topology::Tuple<N> & conns) const
  {
    size_t seed = 0;
    for (size_t i = 0; i < N; ++i) {
      hashing::hash_combine(seed, conns.getc(i));
    }

    return seed;
  }
};

// Overload the redirect operator to output an Tuple<N> object
template <unsigned int N>
std::ostream & operator<< (std::ostream & o, 
                           const meshlib::topology::Tuple<N> Tuple)
{
  return o << Tuple.str();
}

} // namespace std


#endif  // MESHLIB_TOPOLOGY_TUPLE_HPP_
