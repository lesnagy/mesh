#ifndef MESHLIB_TOPOLOGY_TYPES_H_
#define MESHLIB_TOPOLOGY_TYPES_H_

#include <unordered_map>
#include <vector>
#include <set>

namespace meshlib {

namespace topology {

typedef std::unordered_map< unsigned int, std::set<unsigned int> > int_to_intset;

} // namespace topology

} // namespace meshlib

#endif  // MESHLIB_TOPOLOGY_TYPES_H_
