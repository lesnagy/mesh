#include <iostream>
#include <unordered_map>

#include "mesh/Mesh.hpp"

#include "gtest/gtest.h"

namespace {

class MeshTest : public ::testing::Test
{
protected:
  MeshTest()
  {}

  virtual ~MeshTest()
  {}

  virtual void SetUp()
  {}

  virtual void TearDown()
  {}
};

// Test creation of our standard two model mesh.
TEST_F(MeshTest, createMesh)
{
  using namespace meshlib::mesh;
  using namespace meshlib::topology;

  Mesh<4> mesh;

  mesh.begin();

  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});
  mesh.vertex({1, 1, 1});

  SConn<4>   conn0({ 0,  1,  2,  3}); mesh.sconn(  conn0);
  SConn<4>   conn1({ 0,  4,  5,  3}); mesh.sconn(  conn1);
  SConn<4>   conn2({ 6,  7,  0,  8}); mesh.sconn(  conn2);
  SConn<4>   conn3({ 9,  0,  7, 10}); mesh.sconn(  conn3);
  SConn<4>   conn4({ 0,  4, 11,  5}); mesh.sconn(  conn4);
  SConn<4>   conn5({ 0,  9, 11, 10}); mesh.sconn(  conn5);
  SConn<4>   conn6({ 9,  6,  7,  0}); mesh.sconn(  conn6);
  SConn<4>   conn7({ 1, 12,  2,  3}); mesh.sconn(  conn7);
  SConn<4>   conn8({ 1, 12,  3, 13}); mesh.sconn(  conn8);
  SConn<4>   conn9({ 9,  1, 14,  6}); mesh.sconn(  conn9);
  SConn<4>  conn10({ 6,  1, 14,  2}); mesh.sconn( conn10);
  SConn<4>  conn11({ 1, 15, 12, 13}); mesh.sconn( conn11);
  SConn<4>  conn12({14,  9,  7, 16}); mesh.sconn( conn12);
  SConn<4>  conn13({14,  9, 16,  1}); mesh.sconn( conn13);
  SConn<4>  conn14({ 0, 17,  5, 11}); mesh.sconn( conn14);
  SConn<4>  conn15({16,  9, 15,  1}); mesh.sconn( conn15);
  SConn<4>  conn16({ 6,  9,  7, 14}); mesh.sconn( conn16);
  SConn<4>  conn17({14, 12, 18,  2}); mesh.sconn( conn17);
  SConn<4>  conn18({14,  2, 18, 19}); mesh.sconn( conn18);
  SConn<4>  conn19({14, 19,  6,  2}); mesh.sconn( conn19);
  SConn<4>  conn20({20, 21,  7, 22}); mesh.sconn( conn20);
  SConn<4>  conn21({ 2,  0,  6,  1}); mesh.sconn( conn21);
  SConn<4>  conn22({ 0,  2,  6,  4}); mesh.sconn( conn22);
  SConn<4>  conn23({ 6, 23,  4,  2}); mesh.sconn( conn23);
  SConn<4>  conn24({14, 21,  6, 19}); mesh.sconn( conn24);
  SConn<4>  conn25({ 6, 23,  2, 19}); mesh.sconn( conn25);
  SConn<4>  conn26({24, 16,  9, 15}); mesh.sconn( conn26);
  SConn<4>  conn27({ 1,  9, 15, 17}); mesh.sconn( conn27);
  SConn<4>  conn28({ 0,  4,  8, 11}); mesh.sconn( conn28);
  SConn<4>  conn29({ 0,  9,  1, 17}); mesh.sconn( conn29);
  SConn<4>  conn30({ 1, 12, 14,  2}); mesh.sconn( conn30);
  SConn<4>  conn31({ 9, 25, 10,  7}); mesh.sconn( conn31);
  SConn<4>  conn32({ 7, 14, 21,  6}); mesh.sconn( conn32);
  SConn<4>  conn33({ 1,  0,  6,  9}); mesh.sconn( conn33);
  SConn<4>  conn34({ 7, 20,  8,  6}); mesh.sconn( conn34);
  SConn<4>  conn35({ 6, 19, 20, 23}); mesh.sconn( conn35);
  SConn<4>  conn36({ 1, 15, 13, 17}); mesh.sconn( conn36);
  SConn<4>  conn37({11, 25, 10,  9}); mesh.sconn( conn37);
  SConn<4>  conn38({ 0,  4,  6,  8}); mesh.sconn( conn38);
  SConn<4>  conn39({14, 26, 18, 16}); mesh.sconn( conn39);
  SConn<4>  conn40({ 6, 21,  7, 20}); mesh.sconn( conn40);
  SConn<4>  conn41({22, 20,  8,  7}); mesh.sconn( conn41);
  SConn<4>  conn42({ 7, 21, 27, 28}); mesh.sconn( conn42);
  SConn<4>  conn43({ 9, 15, 25, 29}); mesh.sconn( conn43);
  SConn<4>  conn44({ 0, 11,  9, 17}); mesh.sconn( conn44);
  SConn<4>  conn45({ 9, 29, 25, 24}); mesh.sconn( conn45);
  SConn<4>  conn46({ 0,  7, 10,  8}); mesh.sconn( conn46);
  SConn<4>  conn47({ 1, 14, 12, 16}); mesh.sconn( conn47);
  SConn<4>  conn48({ 0,  3,  5, 17}); mesh.sconn( conn48);
  SConn<4>  conn49({27, 26, 21,  7}); mesh.sconn( conn49);
  SConn<4>  conn50({14, 19, 18, 26}); mesh.sconn( conn50);
  SConn<4>  conn51({ 0,  1,  3, 17}); mesh.sconn( conn51);
  SConn<4>  conn52({ 9, 25, 17, 11}); mesh.sconn( conn52);
  SConn<4>  conn53({ 6,  8,  4, 23}); mesh.sconn( conn53);
  SConn<4>  conn54({ 9,  7, 16, 24}); mesh.sconn( conn54);
  SConn<4>  conn55({26, 16,  7, 27}); mesh.sconn( conn55);
  SConn<4>  conn56({ 1, 17, 13,  3}); mesh.sconn( conn56);
  SConn<4>  conn57({ 6, 23, 20,  8}); mesh.sconn( conn57);
  SConn<4>  conn58({ 0,  2,  4,  3}); mesh.sconn( conn58);
  SConn<4>  conn59({24,  7, 16, 27}); mesh.sconn( conn59);
  SConn<4>  conn60({ 0, 10, 11,  8}); mesh.sconn( conn60);
  SConn<4>  conn61({ 9, 24, 25,  7}); mesh.sconn( conn61);
  SConn<4>  conn62({10,  8,  7, 22}); mesh.sconn( conn62);
  SConn<4>  conn63({14, 19, 26, 21}); mesh.sconn( conn63);
  SConn<4>  conn64({ 7, 28, 22, 21}); mesh.sconn( conn64);
  SConn<4>  conn65({14, 16,  7, 26}); mesh.sconn( conn65);
  SConn<4>  conn66({ 7, 26, 21, 14}); mesh.sconn( conn66);
  SConn<4>  conn67({30, 11, 25, 10}); mesh.sconn( conn67);
  SConn<4>  conn68({14, 18, 12, 16}); mesh.sconn( conn68);
  SConn<4>  conn69({ 9, 24, 15, 29}); mesh.sconn( conn69);
  SConn<4>  conn70({ 1, 12, 15, 16}); mesh.sconn( conn70);
  SConn<4>  conn71({ 6, 19, 21, 20}); mesh.sconn( conn71);
  SConn<4>  conn72({ 9, 15, 17, 25}); mesh.sconn( conn72);
  SConn<4>  conn73({31, 32, 33, 34}); mesh.sconn( conn73);
  SConn<4>  conn74({31, 35, 36, 33}); mesh.sconn( conn74);
  SConn<4>  conn75({37, 38, 39, 31}); mesh.sconn( conn75);
  SConn<4>  conn76({40, 41, 42, 43}); mesh.sconn( conn76);
  SConn<4>  conn77({31, 35, 44, 36}); mesh.sconn( conn77);
  SConn<4>  conn78({45, 37, 39, 44}); mesh.sconn( conn78);
  SConn<4>  conn79({37, 39, 44, 31}); mesh.sconn( conn79);
  SConn<4>  conn80({34, 32, 33, 46}); mesh.sconn( conn80);
  SConn<4>  conn81({34, 46, 33, 47}); mesh.sconn( conn81);
  SConn<4>  conn82({31, 43, 37, 38}); mesh.sconn( conn82);
  SConn<4>  conn83({31, 32, 43, 38}); mesh.sconn( conn83);
  SConn<4>  conn84({34, 48, 46, 47}); mesh.sconn( conn84);
  SConn<4>  conn85({37, 42, 40, 49}); mesh.sconn( conn85);
  SConn<4>  conn86({34, 43, 40, 37}); mesh.sconn( conn86);
  SConn<4>  conn87({34, 37, 40, 48}); mesh.sconn( conn87);
  SConn<4>  conn88({50, 34, 43, 40}); mesh.sconn( conn88);
  SConn<4>  conn89({43, 38, 42, 37}); mesh.sconn( conn89);
  SConn<4>  conn90({43, 46, 51, 32}); mesh.sconn( conn90);
  SConn<4>  conn91({43, 32, 51, 52}); mesh.sconn( conn91);
  SConn<4>  conn92({43, 52, 38, 32}); mesh.sconn( conn92);
  SConn<4>  conn93({34, 53, 47, 33}); mesh.sconn( conn93);
  SConn<4>  conn94({32, 43, 34, 31}); mesh.sconn( conn94);
  SConn<4>  conn95({31, 32, 38, 35}); mesh.sconn( conn95);
  SConn<4>  conn96({38, 35, 54, 39}); mesh.sconn( conn96);
  SConn<4>  conn97({43, 42, 38, 52}); mesh.sconn( conn97);
  SConn<4>  conn98({38, 54, 32, 52}); mesh.sconn( conn98);
  SConn<4>  conn99({34, 48, 40, 50}); mesh.sconn( conn99);
  SConn<4> conn100({34, 37, 48, 53}); mesh.sconn(conn100);
  SConn<4> conn101({31, 35, 39, 44}); mesh.sconn(conn101);
  SConn<4> conn102({31, 37, 34, 53}); mesh.sconn(conn102);
  SConn<4> conn103({46, 43, 34, 32}); mesh.sconn(conn103);
  SConn<4> conn104({37, 55, 45, 49}); mesh.sconn(conn104);
  SConn<4> conn105({42, 37, 38, 39}); mesh.sconn(conn105);
  SConn<4> conn106({31, 34, 37, 43}); mesh.sconn(conn106);
  SConn<4> conn107({43, 52, 56, 42}); mesh.sconn(conn107);
  SConn<4> conn108({38, 52, 42, 57}); mesh.sconn(conn108);
  SConn<4> conn109({37, 42, 49, 39}); mesh.sconn(conn109);
  SConn<4> conn110({38, 52, 57, 54}); mesh.sconn(conn110);
  SConn<4> conn111({34, 48, 47, 53}); mesh.sconn(conn111);
  SConn<4> conn112({37, 55, 44, 58}); mesh.sconn(conn112);
  SConn<4> conn113({31, 35, 38, 39}); mesh.sconn(conn113);
  SConn<4> conn114({43, 56, 51, 50}); mesh.sconn(conn114);
  SConn<4> conn115({38, 39, 57, 42}); mesh.sconn(conn115);
  SConn<4> conn116({49, 39, 42, 59}); mesh.sconn(conn116);
  SConn<4> conn117({49, 42, 41, 60}); mesh.sconn(conn117);
  SConn<4> conn118({37, 48, 55, 61}); mesh.sconn(conn118);
  SConn<4> conn119({43, 52, 51, 56}); mesh.sconn(conn119);
  SConn<4> conn120({38, 54, 35, 32}); mesh.sconn(conn120);
  SConn<4> conn121({31, 44, 37, 53}); mesh.sconn(conn121);
  SConn<4> conn122({37, 61, 55, 40}); mesh.sconn(conn122);
  SConn<4> conn123({37, 39, 49, 45}); mesh.sconn(conn123);
  SConn<4> conn124({31, 34, 33, 53}); mesh.sconn(conn124);
  SConn<4> conn125({34, 43, 46, 50}); mesh.sconn(conn125);
  SConn<4> conn126({41, 50, 43, 40}); mesh.sconn(conn126);
  SConn<4> conn127({31, 33, 36, 53}); mesh.sconn(conn127);
  SConn<4> conn128({43, 37, 42, 40}); mesh.sconn(conn128);
  SConn<4> conn129({56, 50, 43, 41}); mesh.sconn(conn129);
  SConn<4> conn130({38, 54, 57, 39}); mesh.sconn(conn130);
  SConn<4> conn131({41, 42, 49, 40}); mesh.sconn(conn131);
  SConn<4> conn132({37, 40, 55, 49}); mesh.sconn(conn132);
  SConn<4> conn133({34, 46, 48, 50}); mesh.sconn(conn133);
  SConn<4> conn134({45, 39, 49, 59}); mesh.sconn(conn134);
  SConn<4> conn135({37, 58, 44, 45}); mesh.sconn(conn135);
  SConn<4> conn136({57, 42, 39, 59}); mesh.sconn(conn136);
  SConn<4> conn137({43, 41, 42, 56}); mesh.sconn(conn137);
  SConn<4> conn138({49, 60, 59, 42}); mesh.sconn(conn138);
  SConn<4> conn139({37, 45, 55, 58}); mesh.sconn(conn139);
  SConn<4> conn140({37, 55, 53, 44}); mesh.sconn(conn140);
  SConn<4> conn141({43, 51, 46, 50}); mesh.sconn(conn141);
  SConn<4> conn142({37, 40, 48, 61}); mesh.sconn(conn142);
  SConn<4> conn143({37, 48, 53, 55}); mesh.sconn(conn143);
  SConn<4> conn144({31, 32, 35, 33}); mesh.sconn(conn144);
  SConn<4> conn145({31, 53, 36, 44}); mesh.sconn(conn145);  
  mesh.end();

  const std::vector< Vertex<3> > & verts = mesh.vertices();

  const std::vector< SConn<4> > & elems  = mesh.elements();

  const std::vector< SConn<3> > & belems = mesh.boundaryElements();

  // Elements and their neighbours.
  for (size_t i = 0; i < elems.size(); ++i) {
    const std::set<unsigned int> neighbourElementIndices = 
      mesh.neighbourElementIndices(i);
    
    const SConn<4> & elem = mesh.element(i);

    std::cout << elem << std::endl;

    for (unsigned int j : neighbourElementIndices) {
      const SConn<4> & nelem = mesh.element(j);
      
      std::cout << "    " << nelem << std::endl;
    }
  }

  // Boundary elements and their neighbours.
  for (size_t i = 0; i < belems.size(); ++i) {
    const std::set<unsigned int> neighbourElementIndices = 
      mesh.boundaryNeighbourElementIndices(i);
    
    const SConn<3> & elem = mesh.boundaryElement(i);

    std::cout << elem << std::endl;

    for (unsigned int j : neighbourElementIndices) {
      const SConn<3> & nelem = mesh.boundaryElement(j);
      
      std::cout << "    " << nelem << std::endl;
    }
  }

  // Vertices and their neigbours.
  for (size_t i = 0; i < verts.size(); ++i) {

    std::cout << i << ": ";

    for (unsigned int j : mesh.neighbourVertexIndices(i)) {
      std::cout << j << " ";
    }
    std::cout << std::endl;
  }

  // Boundary vertices and their neighbours.
  for (size_t i = 0; i < verts.size(); ++i) {

    std::cout << i << ": ";

    for (unsigned int j : mesh.boundaryNeighbourVertexIndices(i)) {
      std::cout << j << " ";
    }
    std::cout << std::endl;
  }
}

}  // namespace (anonymous)

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

