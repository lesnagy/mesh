#include <iostream>
#include <vector>
#include <set>

#include "topology/Types.hpp"
#include "topology/SConn.hpp"
#include "topology/SConnNm1_To_SConnN_Index_Set.hpp"
#include "topology/SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity.hpp"

#include "gtest/gtest.h"

namespace {

class SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity_Test : 
  public ::testing::Test
{
protected:
  SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity_Test()
  {}

  virtual ~SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity_Test()
  {}

  virtual void SetUp()
  {}

  virtual void TearDown()
  {}
};

// Test that we get a SConn1 to SConn1 (by SConn2) map if we use a 
// SConnN-1 to {SConnN:index}
TEST_F(SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity_Test, 
       By_SConnNm1_To_SConnN_Index_Set)
{
  using namespace meshlib::topology;

  SConnNm1_To_SConnN_Index_Set<4> map;

  SConn1_Index_To_SConn1_Index_Set_By_SConn2_Connectivity confun;

  SConn<4> conn0({0,1,2,3});
  SConn<4> conn1({0,1,2,4});
  SConn<4> conn2({5,1,2,4});

  map.insert(conn0);
  map.insert(conn1);
  map.insert(conn2);

  
  int_to_intset omap = confun(map);

  for (auto s : omap) {
    for (auto v : s.second) {
      std::cout << v << ", ";
    }
    std::cout << std::endl;
  }
 
}

}  // namespace (anonymous)

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

