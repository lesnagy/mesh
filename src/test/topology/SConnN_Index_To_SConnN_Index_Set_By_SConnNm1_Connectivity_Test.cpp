#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <algorithm>

#include "topology/Types.hpp"
#include "topology/SConn.hpp"
#include "topology/SConnNm1_To_SConnN_Index_Set.hpp"
#include "topology/SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity.hpp"

#include "gtest/gtest.h"

namespace {

class SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity_Test : 
  public ::testing::Test
{
protected:
  SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity_Test()
  {}

  virtual ~SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity_Test()
  {}

  virtual void SetUp()
  {}

  virtual void TearDown()
  {}
};

// Test that we get a SConn1 to SConn1 (by SConn2) map if we use a 
// SConnN-1 to {SConnN:index}
TEST_F(SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity_Test, 
       By_SConnNm1_To_SConnN_Index_Set)
{
  using namespace meshlib::topology;
  
  SConnNm1_To_SConnN_Index_Set<4> map;

  SConnN_Index_To_SConnN_Index_Set_By_SConnNm1_Connectivity confun;

  int_to_intset expected;
  expected[  0] = { 21,  51,  58,   7}; // 0
  expected[  1] = {      58,  48,   4}; // 1
  expected[  2] = {  6,  38,  46,  34}; // 2
  expected[  3] = {  6,  46,   5,  31}; // 3
  expected[  4] = {       1,  28,  14}; // 4
  expected[  5] = {  3,  44,  60,  37}; // 5
  expected[  6] = {  2,  33,   3,  16}; // 6
  expected[  7] = {       0,  30,   8}; // 7
  expected[  8] = {       7,  56,  11}; // 8
  expected[  9] = { 33,  10,  13,  16}; // 9
  expected[ 10] = { 21,  30,   9,  19}; // 10
  expected[ 11] = {       8,  70,  36}; // 11
  expected[ 12] = { 16,  54,  65,  13}; // 12
  expected[ 13] = {  9,  15,  47,  12}; // 13
  expected[ 14] = {       4,  48,  44}; // 14
  expected[ 15] = { 27,  13,  70,  26}; // 15
  expected[ 16] = {  6,  32,   9,  12}; // 16
  expected[ 17] = {      30,  18,  68}; // 17
  expected[ 18] = {      17,  19,  50}; // 18
  expected[ 19] = { 10,  25,  18,  24}; // 19
  expected[ 20] = {      40,  41,  64}; // 20
  expected[ 21] = {  0,  33,  22,  10}; // 21
  expected[ 22] = { 58,  21,  38,  23}; // 22
  expected[ 23] = {      22,  25,  53}; // 23
  expected[ 24] = { 19,  32,  71,  63}; // 24
  expected[ 25] = {      19,  23,  35}; // 25
  expected[ 26] = {      15,  69,  54}; // 26
  expected[ 27] = { 15,  29,  36,  72}; // 27
  expected[ 28] = {      38,   4,  60}; // 28
  expected[ 29] = { 33,  51,  44,  27}; // 29
  expected[ 30] = {  7,  10,  47,  17}; // 30
  expected[ 31] = {       3,  61,  37}; // 31
  expected[ 32] = { 16,  40,  24,  66}; // 32
  expected[ 33] = { 21,  29,   6,   9}; // 33
  expected[ 34] = {  2,  40,  57,  41}; // 34
  expected[ 35] = {      71,  25,  57}; // 35
  expected[ 36] = {      11,  56,  27}; // 36
  expected[ 37] = {  5,  31,  52,  67}; // 37
  expected[ 38] = { 22,  28,   2,  53}; // 38
  expected[ 39] = {      68,  65,  50}; // 39
  expected[ 40] = { 34,  32,  71,  20}; // 40
  expected[ 41] = {      34,  62,  20}; // 41
  expected[ 42] = {           49,  64}; // 42
  expected[ 43] = {      72,  69,  45}; // 43
  expected[ 44] = {  5,  29,  14,  52}; // 44
  expected[ 45] = {      61,  69,  43}; // 45
  expected[ 46] = {  2,   3,  60,  62}; // 46
  expected[ 47] = { 30,  70,  13,  68}; // 47
  expected[ 48] = {       1,  51,  14}; // 48
  expected[ 49] = {      66,  42,  55}; // 49
  expected[ 50] = {      18,  39,  63}; // 50
  expected[ 51] = {  0,  29,  48,  56}; // 51
  expected[ 52] = {      44,  37,  72}; // 52
  expected[ 53] = {      38,  23,  57}; // 53
  expected[ 54] = { 12,  61,  59,  26}; // 54
  expected[ 55] = {      65,  59,  49}; // 55
  expected[ 56] = {       8,  51,  36}; // 56
  expected[ 57] = {      34,  53,  35}; // 57
  expected[ 58] = {       0,  22,   1}; // 58
  expected[ 59] = {           54,  55}; // 59
  expected[ 60] = {      46,  28,   5}; // 60
  expected[ 61] = {      54,  31,  45}; // 61
  expected[ 62] = {           46,  41}; // 62
  expected[ 63] = {      24,  50,  66}; // 63
  expected[ 64] = {           20,  42}; // 64
  expected[ 65] = { 12,  66,  55,  39}; // 65
  expected[ 66] = { 32,  65,  49,  63}; // 66
  expected[ 67] = {                37}; // 67
  expected[ 68] = {      47,  17,  39}; // 68
  expected[ 69] = {      26,  43,  45}; // 69
  expected[ 70] = {      11,  47,  15}; // 70
  expected[ 71] = {      35,  24,  40}; // 71
  expected[ 72] = {      27,  43,  52}; // 72
  expected[ 73] = {144,  94, 124,  80}; // 73
  expected[ 74] = {     144, 127,  77}; // 74
  expected[ 75] = { 82,  79, 113, 105}; // 75
  expected[ 76] = {131, 126, 128, 137}; // 76
  expected[ 77] = {      74, 101, 145}; // 77
  expected[ 78] = {      79, 123, 135}; // 78
  expected[ 79] = { 75, 121, 101,  78}; // 79
  expected[ 80] = {      73, 103,  81}; // 80
  expected[ 81] = {      80,  93,  84}; // 81
  expected[ 82] = { 75, 106,  83,  89}; // 82
  expected[ 83] = { 95,  94,  82,  92}; // 83
  expected[ 84] = {      81, 133, 111}; // 84
  expected[ 85] = {128, 132, 109, 131}; // 85
  expected[ 86] = { 87, 106,  88, 128}; // 86
  expected[ 87] = { 86, 100,  99, 142}; // 87
  expected[ 88] = { 86,  99, 125, 126}; // 88
  expected[ 89] = {105,  82, 128,  97}; // 89
  expected[ 90] = {     103,  91, 141}; // 90
  expected[ 91] = {      90,  92, 119}; // 91
  expected[ 92] = { 83,  98,  91,  97}; // 92
  expected[ 93] = {      81, 124, 111}; // 93
  expected[ 94] = { 73,  83, 106, 103}; // 94
  expected[ 95] = {144,  83, 113, 120}; // 95
  expected[ 96] = {     113, 120, 130}; // 96
  expected[ 97] = { 89, 108,  92, 107}; // 97
  expected[ 98] = {      92, 120, 110}; // 98
  expected[ 99] = {      87,  88, 133}; // 99
  expected[100] = { 87, 102, 111, 143}; // 100
  expected[101] = {     113,  77,  79}; // 101
  expected[102] = {106, 124, 121, 100}; // 102
  expected[103] = { 94,  80,  90, 125}; // 103
  expected[104] = {     123, 139, 132}; // 104
  expected[105] = { 75,  89, 109, 115}; // 105
  expected[106] = {102,  94,  82,  86}; // 106
  expected[107] = {      97, 137, 119}; // 107
  expected[108] = {      97, 115, 110}; // 108
  expected[119] = {105, 123,  85, 116}; // 109
  expected[110] = {      98, 108, 130}; // 110
  expected[111] = {      84,  93, 100}; // 111
  expected[112] = {     140, 135, 139}; // 112
  expected[113] = { 95, 101,  75,  96}; // 113
  expected[114] = {     141, 129, 119}; // 114
  expected[115] = {105, 130, 108, 136}; // 115
  expected[116] = {109, 136, 134, 138}; // 116
  expected[117] = {          131, 138}; // 117
  expected[118] = {     143, 142, 122}; // 118
  expected[119] = {      91, 114, 107}; // 119
  expected[120] = {      95,  98,  96}; // 120
  expected[121] = { 79, 102, 145, 140}; // 121
  expected[122] = {     132, 142, 118}; // 122
  expected[123] = { 78, 109, 104, 134}; // 123
  expected[124] = { 73, 127, 102,  93}; // 124
  expected[125] = {103,  88, 133, 141}; // 125
  expected[126] = {      76,  88, 129}; // 126
  expected[137] = {      74, 124, 145}; // 127
  expected[138] = { 85,  86,  89,  76}; // 128
  expected[139] = {     126, 137, 114}; // 129
  expected[130] = {      96, 115, 110}; // 130
  expected[131] = {      76,  85, 117}; // 131
  expected[132] = {      85, 122, 104}; // 132
  expected[133] = {      84, 125,  99}; // 133
  expected[134] = {          123, 116}; // 134
  expected[135] = {      78, 112, 139}; // 135
  expected[136] = {          115, 116}; // 136
  expected[137] = {      76, 129, 107}; // 137
  expected[138] = {          116, 117}; // 138
  expected[139] = {     104, 135, 112}; // 139
  expected[140] = {     121, 112, 143}; // 140
  expected[141] = {     125,  90, 114}; // 141
  expected[142] = {      87, 122, 118}; // 142
  expected[143] = {     100, 118, 140}; // 143
  expected[144] = {      73,  95,  74}; // 144
  expected[145] = {      77, 127, 121}; // 145

  SConn<4>   conn0({ 0,  1,  2,  3}); map.insert(  conn0);
  SConn<4>   conn1({ 0,  4,  5,  3}); map.insert(  conn1);
  SConn<4>   conn2({ 6,  7,  0,  8}); map.insert(  conn2);
  SConn<4>   conn3({ 9,  0,  7, 10}); map.insert(  conn3);
  SConn<4>   conn4({ 0,  4, 11,  5}); map.insert(  conn4);
  SConn<4>   conn5({ 0,  9, 11, 10}); map.insert(  conn5);
  SConn<4>   conn6({ 9,  6,  7,  0}); map.insert(  conn6);
  SConn<4>   conn7({ 1, 12,  2,  3}); map.insert(  conn7);
  SConn<4>   conn8({ 1, 12,  3, 13}); map.insert(  conn8);
  SConn<4>   conn9({ 9,  1, 14,  6}); map.insert(  conn9);
  SConn<4>  conn10({ 6,  1, 14,  2}); map.insert( conn10);
  SConn<4>  conn11({ 1, 15, 12, 13}); map.insert( conn11);
  SConn<4>  conn12({14,  9,  7, 16}); map.insert( conn12);
  SConn<4>  conn13({14,  9, 16,  1}); map.insert( conn13);
  SConn<4>  conn14({ 0, 17,  5, 11}); map.insert( conn14);
  SConn<4>  conn15({16,  9, 15,  1}); map.insert( conn15);
  SConn<4>  conn16({ 6,  9,  7, 14}); map.insert( conn16);
  SConn<4>  conn17({14, 12, 18,  2}); map.insert( conn17);
  SConn<4>  conn18({14,  2, 18, 19}); map.insert( conn18);
  SConn<4>  conn19({14, 19,  6,  2}); map.insert( conn19);
  SConn<4>  conn20({20, 21,  7, 22}); map.insert( conn20);
  SConn<4>  conn21({ 2,  0,  6,  1}); map.insert( conn21);
  SConn<4>  conn22({ 0,  2,  6,  4}); map.insert( conn22);
  SConn<4>  conn23({ 6, 23,  4,  2}); map.insert( conn23);
  SConn<4>  conn24({14, 21,  6, 19}); map.insert( conn24);
  SConn<4>  conn25({ 6, 23,  2, 19}); map.insert( conn25);
  SConn<4>  conn26({24, 16,  9, 15}); map.insert( conn26);
  SConn<4>  conn27({ 1,  9, 15, 17}); map.insert( conn27);
  SConn<4>  conn28({ 0,  4,  8, 11}); map.insert( conn28);
  SConn<4>  conn29({ 0,  9,  1, 17}); map.insert( conn29);
  SConn<4>  conn30({ 1, 12, 14,  2}); map.insert( conn30);
  SConn<4>  conn31({ 9, 25, 10,  7}); map.insert( conn31);
  SConn<4>  conn32({ 7, 14, 21,  6}); map.insert( conn32);
  SConn<4>  conn33({ 1,  0,  6,  9}); map.insert( conn33);
  SConn<4>  conn34({ 7, 20,  8,  6}); map.insert( conn34);
  SConn<4>  conn35({ 6, 19, 20, 23}); map.insert( conn35);
  SConn<4>  conn36({ 1, 15, 13, 17}); map.insert( conn36);
  SConn<4>  conn37({11, 25, 10,  9}); map.insert( conn37);
  SConn<4>  conn38({ 0,  4,  6,  8}); map.insert( conn38);
  SConn<4>  conn39({14, 26, 18, 16}); map.insert( conn39);
  SConn<4>  conn40({ 6, 21,  7, 20}); map.insert( conn40);
  SConn<4>  conn41({22, 20,  8,  7}); map.insert( conn41);
  SConn<4>  conn42({ 7, 21, 27, 28}); map.insert( conn42);
  SConn<4>  conn43({ 9, 15, 25, 29}); map.insert( conn43);
  SConn<4>  conn44({ 0, 11,  9, 17}); map.insert( conn44);
  SConn<4>  conn45({ 9, 29, 25, 24}); map.insert( conn45);
  SConn<4>  conn46({ 0,  7, 10,  8}); map.insert( conn46);
  SConn<4>  conn47({ 1, 14, 12, 16}); map.insert( conn47);
  SConn<4>  conn48({ 0,  3,  5, 17}); map.insert( conn48);
  SConn<4>  conn49({27, 26, 21,  7}); map.insert( conn49);
  SConn<4>  conn50({14, 19, 18, 26}); map.insert( conn50);
  SConn<4>  conn51({ 0,  1,  3, 17}); map.insert( conn51);
  SConn<4>  conn52({ 9, 25, 17, 11}); map.insert( conn52);
  SConn<4>  conn53({ 6,  8,  4, 23}); map.insert( conn53);
  SConn<4>  conn54({ 9,  7, 16, 24}); map.insert( conn54);
  SConn<4>  conn55({26, 16,  7, 27}); map.insert( conn55);
  SConn<4>  conn56({ 1, 17, 13,  3}); map.insert( conn56);
  SConn<4>  conn57({ 6, 23, 20,  8}); map.insert( conn57);
  SConn<4>  conn58({ 0,  2,  4,  3}); map.insert( conn58);
  SConn<4>  conn59({24,  7, 16, 27}); map.insert( conn59);
  SConn<4>  conn60({ 0, 10, 11,  8}); map.insert( conn60);
  SConn<4>  conn61({ 9, 24, 25,  7}); map.insert( conn61);
  SConn<4>  conn62({10,  8,  7, 22}); map.insert( conn62);
  SConn<4>  conn63({14, 19, 26, 21}); map.insert( conn63);
  SConn<4>  conn64({ 7, 28, 22, 21}); map.insert( conn64);
  SConn<4>  conn65({14, 16,  7, 26}); map.insert( conn65);
  SConn<4>  conn66({ 7, 26, 21, 14}); map.insert( conn66);
  SConn<4>  conn67({30, 11, 25, 10}); map.insert( conn67);
  SConn<4>  conn68({14, 18, 12, 16}); map.insert( conn68);
  SConn<4>  conn69({ 9, 24, 15, 29}); map.insert( conn69);
  SConn<4>  conn70({ 1, 12, 15, 16}); map.insert( conn70);
  SConn<4>  conn71({ 6, 19, 21, 20}); map.insert( conn71);
  SConn<4>  conn72({ 9, 15, 17, 25}); map.insert( conn72);
  SConn<4>  conn73({31, 32, 33, 34}); map.insert( conn73);
  SConn<4>  conn74({31, 35, 36, 33}); map.insert( conn74);
  SConn<4>  conn75({37, 38, 39, 31}); map.insert( conn75);
  SConn<4>  conn76({40, 41, 42, 43}); map.insert( conn76);
  SConn<4>  conn77({31, 35, 44, 36}); map.insert( conn77);
  SConn<4>  conn78({45, 37, 39, 44}); map.insert( conn78);
  SConn<4>  conn79({37, 39, 44, 31}); map.insert( conn79);
  SConn<4>  conn80({34, 32, 33, 46}); map.insert( conn80);
  SConn<4>  conn81({34, 46, 33, 47}); map.insert( conn81);
  SConn<4>  conn82({31, 43, 37, 38}); map.insert( conn82);
  SConn<4>  conn83({31, 32, 43, 38}); map.insert( conn83);
  SConn<4>  conn84({34, 48, 46, 47}); map.insert( conn84);
  SConn<4>  conn85({37, 42, 40, 49}); map.insert( conn85);
  SConn<4>  conn86({34, 43, 40, 37}); map.insert( conn86);
  SConn<4>  conn87({34, 37, 40, 48}); map.insert( conn87);
  SConn<4>  conn88({50, 34, 43, 40}); map.insert( conn88);
  SConn<4>  conn89({43, 38, 42, 37}); map.insert( conn89);
  SConn<4>  conn90({43, 46, 51, 32}); map.insert( conn90);
  SConn<4>  conn91({43, 32, 51, 52}); map.insert( conn91);
  SConn<4>  conn92({43, 52, 38, 32}); map.insert( conn92);
  SConn<4>  conn93({34, 53, 47, 33}); map.insert( conn93);
  SConn<4>  conn94({32, 43, 34, 31}); map.insert( conn94);
  SConn<4>  conn95({31, 32, 38, 35}); map.insert( conn95);
  SConn<4>  conn96({38, 35, 54, 39}); map.insert( conn96);
  SConn<4>  conn97({43, 42, 38, 52}); map.insert( conn97);
  SConn<4>  conn98({38, 54, 32, 52}); map.insert( conn98);
  SConn<4>  conn99({34, 48, 40, 50}); map.insert( conn99);
  SConn<4> conn100({34, 37, 48, 53}); map.insert(conn100);
  SConn<4> conn101({31, 35, 39, 44}); map.insert(conn101);
  SConn<4> conn102({31, 37, 34, 53}); map.insert(conn102);
  SConn<4> conn103({46, 43, 34, 32}); map.insert(conn103);
  SConn<4> conn104({37, 55, 45, 49}); map.insert(conn104);
  SConn<4> conn105({42, 37, 38, 39}); map.insert(conn105);
  SConn<4> conn106({31, 34, 37, 43}); map.insert(conn106);
  SConn<4> conn107({43, 52, 56, 42}); map.insert(conn107);
  SConn<4> conn108({38, 52, 42, 57}); map.insert(conn108);
  SConn<4> conn109({37, 42, 49, 39}); map.insert(conn109);
  SConn<4> conn110({38, 52, 57, 54}); map.insert(conn110);
  SConn<4> conn111({34, 48, 47, 53}); map.insert(conn111);
  SConn<4> conn112({37, 55, 44, 58}); map.insert(conn112);
  SConn<4> conn113({31, 35, 38, 39}); map.insert(conn113);
  SConn<4> conn114({43, 56, 51, 50}); map.insert(conn114);
  SConn<4> conn115({38, 39, 57, 42}); map.insert(conn115);
  SConn<4> conn116({49, 39, 42, 59}); map.insert(conn116);
  SConn<4> conn117({49, 42, 41, 60}); map.insert(conn117);
  SConn<4> conn118({37, 48, 55, 61}); map.insert(conn118);
  SConn<4> conn119({43, 52, 51, 56}); map.insert(conn119);
  SConn<4> conn120({38, 54, 35, 32}); map.insert(conn120);
  SConn<4> conn121({31, 44, 37, 53}); map.insert(conn121);
  SConn<4> conn122({37, 61, 55, 40}); map.insert(conn122);
  SConn<4> conn123({37, 39, 49, 45}); map.insert(conn123);
  SConn<4> conn124({31, 34, 33, 53}); map.insert(conn124);
  SConn<4> conn125({34, 43, 46, 50}); map.insert(conn125);
  SConn<4> conn126({41, 50, 43, 40}); map.insert(conn126);
  SConn<4> conn127({31, 33, 36, 53}); map.insert(conn127);
  SConn<4> conn128({43, 37, 42, 40}); map.insert(conn128);
  SConn<4> conn129({56, 50, 43, 41}); map.insert(conn129);
  SConn<4> conn130({38, 54, 57, 39}); map.insert(conn130);
  SConn<4> conn131({41, 42, 49, 40}); map.insert(conn131);
  SConn<4> conn132({37, 40, 55, 49}); map.insert(conn132);
  SConn<4> conn133({34, 46, 48, 50}); map.insert(conn133);
  SConn<4> conn134({45, 39, 49, 59}); map.insert(conn134);
  SConn<4> conn135({37, 58, 44, 45}); map.insert(conn135);
  SConn<4> conn136({57, 42, 39, 59}); map.insert(conn136);
  SConn<4> conn137({43, 41, 42, 56}); map.insert(conn137);
  SConn<4> conn138({49, 60, 59, 42}); map.insert(conn138);
  SConn<4> conn139({37, 45, 55, 58}); map.insert(conn139);
  SConn<4> conn140({37, 55, 53, 44}); map.insert(conn140);
  SConn<4> conn141({43, 51, 46, 50}); map.insert(conn141);
  SConn<4> conn142({37, 40, 48, 61}); map.insert(conn142);
  SConn<4> conn143({37, 48, 53, 55}); map.insert(conn143);
  SConn<4> conn144({31, 32, 35, 33}); map.insert(conn144);
  SConn<4> conn145({31, 53, 36, 44}); map.insert(conn145);

  DEBUG("Creating omap using confun");
  int_to_intset omap = confun(map);
  DEBUG("OK!");

  for (size_t i = 0; i < omap.size(); ++i) {
    auto exp = expected[i];
    auto act = omap[i];

    bool testResult = std::equal(exp.begin(), exp.end(), act.begin());

    if (testResult != true) {
      size_t j = 0;
      std::stringstream ssExpected;
      std::stringstream ssActual;

      j = 0;
      ssExpected << "[" << std::setw(3) << i << "] {";
      for (auto v : exp) {
        if (j != exp.size()-1) {
          ssExpected << std::setw(3) << v << ", ";
        } else {
          ssExpected << std::setw(3) << v;
        }
        j = j + 1;
      }
      ssExpected << "}";

      j = 0;
      ssActual << "[" << std::setw(3) << i << "] {";
      for (auto v : act) {
        if (j != act.size()-1) {
          ssActual << std::setw(3) << v << ", ";
        } else {
          ssActual << std::setw(3) << v;
        }
        j = j + 1;
      }
      ssActual << "}";

      std::cout << "  Actual: " << ssActual.str()   << std::endl;
      std::cout << "Expected: " << ssExpected.str() << std::endl;

    }

    ASSERT_TRUE(testResult);
  }
 
}

}  // namespace (anonymous)

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

