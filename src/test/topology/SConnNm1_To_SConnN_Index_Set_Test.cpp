#include <cstdio>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>

#include "topology/SConn.hpp"
#include "topology/SConnNm1_To_SConnN_Index_Set.hpp"

#include "gtest/gtest.h"

namespace {

class SConnNm1_To_SConnN_Index_Set_Test : public ::testing::Test 
{
protected:
 SConnNm1_To_SConnN_Index_Set_Test()
  {}

  virtual ~SConnNm1_To_SConnN_Index_Set_Test()
  {}

  virtual void SetUp()
  {}

  virtual void TearDown()
  {}
};

// Test FaceToElementIndexSet.insert() function.
TEST_F(SConnNm1_To_SConnN_Index_Set_Test, methodInsert)
{
  using namespace meshlib::topology;
  
  std::unordered_map< SConn<3>, std::set<unsigned int> > expected;
  expected.insert({SConn<3>({2,5,6}), {2}});
  expected.insert({SConn<3>({2,3,6}), {2}});
  expected.insert({SConn<3>({3,5,6}), {2}});
  expected.insert({SConn<3>({2,3,5}), {1,2}});
  expected.insert({SConn<3>({1,2,5}), {1}});
  expected.insert({SConn<3>({1,3,4}), {0}});
  expected.insert({SConn<3>({1,2,4}), {0}});
  expected.insert({SConn<3>({1,3,5}), {1}});
  expected.insert({SConn<3>({2,3,4}), {0}});
  expected.insert({SConn<3>({1,2,3}), {0,1}});

  SConnNm1_To_SConnN_Index_Set<4> map;

  SConn<4> conn0({1,2,3,4});
  SConn<4> conn1({1,2,3,5});
  SConn<4> conn2({6,2,3,5});

  map.insert(conn0);
  map.insert(conn1);
  map.insert(conn2);

  for (auto kvpair : map) {
    const SConn<3>               & face   = kvpair.first;
    const std::set<unsigned int> & idxset = kvpair.second;

    DEBUG("Face: " << face);

    // Check that the face is in the expected set.
    ASSERT_TRUE(expected.find(face) != expected.end());

    const std::set<unsigned int> eidxset = expected[face];
    ASSERT_TRUE( std::equal(idxset.begin(), idxset.end(), eidxset.begin()) );
  }
}

}  // namespace (anonymous)

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

