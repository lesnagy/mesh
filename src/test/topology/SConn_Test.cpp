#include <cstdio>

#include <iostream>
#include <unordered_map>

#include "topology/SConn.hpp"

#include "gtest/gtest.h"

namespace {

class SConnTest : public ::testing::Test
{
protected:
  SConnTest()
  {}

  virtual ~SConnTest()
  {}

  virtual void SetUp()
  {}

  virtual void TearDown()
  {}
};

// Test sub conns functionality
TEST_F(SConnTest, methodSubConns)
{
  using namespace meshlib::topology;

  // Create a Conn-4 simplex.
  SConn<4> conn({1,2,3,4});

  // Check that each face is correct.
  unsigned int efaces[4][3] = {
    {1, 2, 3}, {1, 2, 4}, {1, 3, 4}, {2, 3, 4}};

  std::vector< SConn<3> > faces = conn.subConns();

  ASSERT_EQ(faces.size(), 4u);

  for (size_t i = 0; i < 4; ++i) {
    ASSERT_EQ(faces[i].size(), 3u);
    for (size_t j = 0; j < 3; ++j) {
      ASSERT_EQ(faces[i][j], efaces[i][j]);
    }
  }
}

// Given a sequence of 4 indices, expect that the SConn object has the same set
// of indices but in sorted order.
TEST_F(SConnTest, methodConstructor1)
{
  using namespace meshlib::topology;
  
  // Create a conn-4 simple.
  SConn<4> conn({9,2,1,7});

  std::vector<unsigned int> eidxs = {1, 2, 7, 9};

  ASSERT_TRUE( std::equal(conn.begin(), conn.end(), eidxs.begin()) );
}

// Test the equality operator that two SConn objects are the same.
TEST_F(SConnTest, methodEquals1)
{
  using namespace meshlib::topology;
  
  SConn<4> conn1({9,2,1,7});
  SConn<4> conn2({1,9,2,7});

  ASSERT_TRUE(conn1 == conn2);
}

// Test the equality operator that an SConn and a vector are the same.
TEST_F(SConnTest, methodEquals2)
{
  using namespace meshlib::topology;

  SConn<4> conn({9,2,1,7});
  std::vector<unsigned int> vec = {1,2,7,9};

  ASSERT_TRUE(conn == vec);
}

}  // namespace (anonymous)

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
